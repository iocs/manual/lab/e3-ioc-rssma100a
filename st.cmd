# Required modules
require essioc
require rssmx100a

# Load essioc configs
iocshLoad($(essioc_DIR)/common_config.iocsh)

# Signal Generator IP
epicsEnvSet("IP_ADDR","rflab-sma100a.cslab.esss.lu.se")

# PV prefix
epicsEnvSet("P", "LabS-RFLab:")
epicsEnvSet("R", "RFS-SG-001:")

iocshLoad(${rssmx100a_DIR}rssma100a.iocsh, "P=${P},R=${R},IP_ADDR=${IP_ADDR}")

iocInit()
